

jQuery(document).ready(function ($) {

	var add_movie = function(data, playlist_selected, id_movie, id_list)
	{
	let cloned = $('#mv_' + id_movie + '_pl_model_').clone();
	cloned.attr('id', 'mv_' + id_movie + '_pl_' + id_list + '_');
	cloned.text(playlist_selected.text());
	$('#mv_' + id_movie + '_pl_parent_').append(cloned);
	cloned.fadeIn(1000);
};


	$(".movie_playlist_item").on("click", function () {
		var playlist_selected = $(this);
		let id_list = playlist_selected.attr('id_playlist');
		let id_movie = playlist_selected.attr('id_movie');
		$.get("/play/list/" + id_list + "/remove/" + id_movie)
			.done(
				function(data){
					playlist_selected.remove();
			}
			);

	});


	$("#playlist-selection-new-add").on("change", function () {

		let name = $("#playlist-selection-new-add").val();
		let id_movie = $("#playlist-selection").attr('id_movie');

		$.post(
			{
				url: "/playlist/new/add/" + id_movie,
				data: { 'name': name },
				dataType: "json",
				success: function (data) {
					let cloned = $('#mv_' + id_movie + '_pl_model_').clone();
					id_list = data.id_list;
					cloned.attr('id', 'mv_' + id_movie + '_pl_' + id_list + '_');
					cloned.attr('id_movie', id_movie);
					cloned.attr('id_playlist', id_list);
					cloned.text(name);
					$('#mv_' + id_movie + '_pl_parent_').append(cloned);
					cloned.fadeIn(1000);

					$("#playlist-selection-ul").append('<li><div class="playlist-selection-item-add" id_playlist="' + id_list + '">' + name + ' </div></li>');
					$("#with_playlist").append('<option value="' + id_list + '">' + name + ' </option>');
				}
			})
		$("#playlist-selection").hide();
	});

	$('.add-new').click(function (e) {
		var top = e.pageY + 30;
		var left = e.pageX - 90;
		$("#playlist-selection").css({
			display: "block",
			position: "absolute",
			background: "#07c9bffd",
			'border-radius': "14px",
			top: top,
			left: left,
			width: "auto"
		});
		let id_movie = $(e.target).attr('id_movie');
		$("#playlist-selection").attr('id_movie', id_movie);
		$("#playlist-selection").addClass("show");
		return false; //blocks default Webbrowser right click menu
	});

	//$("#playlist-selection").click(function () {$(this).hide();});


	$(".playlist-selection-item-add").on("click", function () {

		let playlist_selected = $(this);
		let id_list = playlist_selected.attr('id_playlist');
		let id_movie = $("#playlist-selection").attr('id_movie');
		$("#playlist-selection-new-add").val("");
		$("#playlist-selection-new-add").text("");
		$.get("/play/list/" + id_list + "/add/" + id_movie)
			.done(
				function(data){let cloned = $('#mv_' + id_movie + '_pl_model_').clone();
				cloned.attr('id', 'mv_' + id_movie + '_pl_' + id_list + '_');
				cloned.attr('id_movie', id_movie);
				cloned.attr('id_playlist', id_list);
				cloned.addClass("movie_playlist_item");
				cloned.text(playlist_selected.text());
				$('#mv_' + id_movie + '_pl_parent_').append(cloned);
				cloned.fadeIn(1000);}


			);
		$("#playlist-selection").hide();
	});

	$('#add_new_list').click(function () {
		let name_el = $('#new_list_name');
		let name = name_el.val();
		$.post({
			url: "/user/playlist/newapi",
			data: { 'name': name },
			success: function (data) { alert(data.status) },
			dataType: "json"
		});
	})


	$('#test_api').click(function () {
		$.get({
			url: "https://api.themoviedb.org/3/movie/22?api_key=83142e4b2c007433690f2685af7e4541&language=fr",
			success: function (data) {
				$('#image_api1').attr('style', "background-image: url('https://image.tmdb.org/t/p/w500/" + data.backdrop_path + "')");
				//alert (data.status);
			},
			dataType: "json"
		});
	})

	$('.add-as-favorite').click(function () {
		var icon_fav = $(this);
		$.get("/favorites/add/" + icon_fav.attr('movie_id'), { icon: icon_fav.val() })
			.done(function (data) {
				icon_fav.toggleClass('movie-is-favorite');
				icon_fav.toggleClass('add-as-favorite');
			});
	})
	$('.movie-is-favorite').click(function () {
		var icon_fav = $(this);
		$.get("/favorites/remove/" + icon_fav.attr('movie_id'), { icon: icon_fav.val() })
			.done(function (data) {
				icon_fav.toggleClass('movie-is-favorite');
				icon_fav.toggleClass('add-as-favorite');
			});
	})

});
<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Controller\PlayListController;
use App\Controller\UserController;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Api;
use App\Entity\ApiRequest;
use App\Repository\UserPlayListRepository;
use App\Repository\PlayListRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class HomeController extends AbstractController
{
    /**
     * @var UserController
     */
    private $user_controller;

    /**
     * @var UserPlayListRepository
     */
    private $user_playlist_repo;

    /**
     * @var PlayListRepository
     */
    private $playlist_repo;

    
    /**
     * @var PlayListController
     */
    private $movies_controller;

    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(UserController $user_controller, PlayListController $movies_controller, PlayListRepository $playlist_repo, EntityManagerInterface $em, UserPlayListRepository $user_playlist_repo)
    {
        
        $this->movies_controller = $movies_controller;
        $this->playlist_repo = $playlist_repo;
        $this->em = $em;
        $this->user_playlist_repo = $user_playlist_repo;
        $this->user_controller = $user_controller;

    }

    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function index(Request $request): Response
    {
        $result_field = 'results';
        if($this->getUser()){
            $user_controller= $this->user_controller;
            $favorite_pl_id = $user_controller->getFavoriteList()->getIdDB();
        }
        else
        $favorite_pl_id = -1;
        
        $page = $request->get('page');
        [$movies_pl_map, $all_playlist] = $this->updatePlayListinSession();
        $movies_controller = $this->movies_controller;
        
        $resource = '/discover/movie';
        $param=['sort_by'=>'popularity.desc'];
        $with_genres = $request->get("with_genres");
        if ( $with_genres!='all')
        {   
            $resource = '/discover/movie';
            $param +=['with_genres' => $request->get("with_genres")];
        }
        $with_keywords = $request->get("with_keywords");
        if ($with_keywords !='')
        {
            //$resource = '/search/multi';
            $resource = '/search/movie';
            $param +=['query' => $request->get("with_keywords")];
        }
        $with_release_year = $request->get("with_release_year");
        if ($with_release_year !='all')
        {
            $resource = '/search/movie';
            $param +=['year' => $with_release_year];
            if ($request->get("with_keywords") !='')
                $param +=['query' => $request->get("with_keywords")];
            else
                $param +=['query' => '*a*'];
        }
        
        $with_playlist = $request->get("with_playlist");
        if (!is_null($with_playlist) && $with_playlist !='all')
        {
            $resource = "/list/$with_playlist";
            $result_field = 'items';
        }
        

        $movies = $movies_controller->getAllOf(['resource' => $resource, 'resource_param' => $param]);
        $genres = $this->getGenres();
        $genre_current = $request->get("genre_selected");
        return $this->render('pages/home.html.twig', [
                                                    'can_search' => true,
                                                    'imagebase' => 'https://image.tmdb.org/t/p/w500',
                                                    'movies' => $movies[$result_field],
                                                    'movies_pl_map' => $movies_pl_map,
                                                    'all_playlist' => $all_playlist,
                                                    'genres'=> $genres,
                                                    'genre_current' => $genre_current,
                                                    'with_keywords'=> $with_keywords,
                                                    'with_release_year'=> $with_release_year,
                                                    'with_genres'=> $with_genres,
                                                    'with_keywords'=> $with_keywords,
                                                    'with_playlist' => $with_playlist,
                                                    'favorite_pl_id' => $favorite_pl_id
                                                    ]);
    }
    /**
     * @Route("/db/auth", name="db.auth")
     */
    public function authentification()
    {
        $api = new Api();
        $api->authentification();
    }

    public function updatePlayListinSession($force=false)
    {
        if (is_null($this->getUser()))
            return [[],[]];

        $session = new Session();
        if ($force || !is_null($session->get('movies_playlist')))
            return [$session->get('movies_playlist'), $session->get('all_playlist')];

        $id_user = $this->getUser()->getId();

        //return true;

        //get the list accessible bu the the user stored in mysql

        $playlist_id_list = $this->user_playlist_repo->findBy(["IdUser" => $id_user]);
        $id_list_db=[];
        $playlist_list = [];
        foreach($playlist_id_list as $playlist_id)
        {
                $pl = $this->playlist_repo->findOneBy(['id' => $playlist_id->getIdPlayList()]);
                $playlist_list[$pl->getIdDB()] = $pl ;
                $id_list_db[] = $pl->getIdDB();
        }
        
        // $id_list_db contains the db id of the list affected to the user
       
        $session->set('playlist', $playlist_list);

        //Loop on id_list_db to retreive the list of the movies included
        //id_list_db
        $this->authentification();
        $api = new Api();
        $movies_map_list=[]; // id_movie => [list of playlist object]
        foreach ($id_list_db as $id_db)
        {
            $request = new ApiRequest();
            $request->setResources("/list/$id_db");
            $result_list = $api->get($request);
            //merge the movies and the list
            foreach ($result_list['items'] as $movie)
            {
                $id_movie = $movie['id'];
                if (!isset($movies_map_list[$id_movie]))
                    $movies_map_list[$id_movie] = [];

                $existing_list = $movies_map_list[$id_movie];
                $existing_list += ['k_' . $id_db => $playlist_list[$id_db]];
                $movies_map_list[$id_movie] = $existing_list;
            }
        }
        $session->set('movies_playlist', $movies_map_list);
        $session->set('all_playlist', $playlist_list);
        return [$movies_map_list, $playlist_list];
    }

    public function getGenres()
    {
        $session = new Session();
        if (!is_null($session->get('movies_genres')))
            return $session->get('movies_genres');
        
        $api = new Api();        
        $request = new ApiRequest();
        $request->setResources("/genre/movie/list");
        $result_list = $api->get($request);
        $session->set('movies_genres', $result_list['genres']);
        return $result_list['genres'];
    }
}

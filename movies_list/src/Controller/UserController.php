<?php

namespace App\Controller;

use App\Entity\Api;
use App\Entity\ApiRequest;
use App\Entity\PlayList;
use App\Entity\User;
use App\Entity\UserPlayList;
use App\Form\UserType;
use App\Repository\PlayListRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use Twig\Environment;

class UserController extends AbstractController
{
    
    /**
     * @var PlayListRepository
     */
    private $repository;

    /**
     * @var PlayListController
     */
    private $play_list_controller;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(PlayListRepository $repository, EntityManagerInterface $em, PlayListController $play_list_controller)//, UserPlayList $user_playlist)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->play_list_controller = $play_list_controller;
        //$this->user_playlist = $user_playlist;
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_logout');
    }

    /**
     * @Route("/user/playlist/newapi", name="user.playlists.new_byapi")
     * @return Response
     */
    public function newPlayList(Request $request): Response
    {
        $play_list_new = $this->createPlayList(['name' => $request->get('name'), 'type_of' => 2]);
        if(!is_null($play_list_new))
        {
            $status = 'ok' ;
            $id = $play_list_new->getId();
            $name = $play_list_new->getName();
        }
        else
        {
            $status = 'ko' ;
            $id = null;
            $name = '';
        }

        $response = new JsonResponse(['status'=> $status, 
                                      'id' => $id,
                                      'name' => $name]);
        return $response;        
    }

    /**
     * @return PlayList
     */

    private function createPlayList($param, $id=null): PlayList
    {
        //Create the movies list
        $id_user=$this->getUser()->getId();
        $param += ['id_user' => $id_user];
        if (!is_null($id))
            $param += ['id' => $id];
        $play_list_new = $this->play_list_controller->newPlayList($param);

        if(!is_null($play_list_new))
        {
            $play_list_new_id = $play_list_new->getId();
            //Add the movies list to the list of movies of the user.
            $this->addPlayList($play_list_new_id, UserPlayList::ROLE_OWNER);
        }
        return $play_list_new;
    }
    /**
     * @Route("/user/playlist/addapi", name="user.playlist.add_byapi")
     
     */
    public function addPlayList($play_list_id, $role)
    {
        $em = $this->em;

        $user_playlist = new UserPlayList();

        $user_playlist->setIdPlayList($play_list_id)
        ->setIdUser($this->getUser()->getId())
        ->setRole($role);
        $em->persist($user_playlist);
        $em->flush();           
    }

    /**
     * @Route("/favorites/add/{id_movie}", name="favorites.add")
     
     */
    public function addFavorite($id_movie)
    {
        $favorites = $this->getFavoriteList();

        $this->play_list_controller->addMovie($favorites, $id_movie);
        $response = new JsonResponse(['status'=> 'ok']);
        return $response;  
    }
    /**
     * @Route("/favorites/remove/{id_movie}", name="favorites.remove")
     
     */
    public function removeFavorite($id_movie)
    {
        $favorites = $this->getFavoriteList();

        $this->play_list_controller->removeMovie($favorites, $id_movie);
        $response = new JsonResponse(['status'=> 'ok']);
        return $response;  
    }


    /**
     * @Route("/playlist/new/add/{id_movie}", name="playlist.new.add")
     
     */
    public function addInNewPlaylist(Request $request, $id_movie)
    {
        
        $id_user=$this->getUser()->getId();
        $name = $request->get('name');
        $repository = $this->repository;
        $already_exist = $repository->findOneBy(["IdOwner" => $id_user,
                                                'TypeOf' => 2,
                                                'Name' => $name]);
        if (!is_null($already_exist))
        {
            $return = ['status_code'=> 'ko', 'status_message'=> 'Playlist named "' .  $name . '" already exist !'];
            $response = new JsonResponse($return);
        }
        
        $playlist_new =  $this->createPlayList(['name' => $name, 'type_of' => 2]);
        //$playlist_new->setDescription("My favorites movies");
        $response = $this->play_list_controller->addMovie($playlist_new, $id_movie);
       
        return $response;  
    }

      /**
     * @return PlayList
     */
    public function getFavoriteList():PlayList
    {
        $repository = $this->repository;
        $id_user=$this->getUser()->getId();
        $already_exist = $repository->findOneBy(["IdOwner" => $id_user,
                                                'TypeOf' => 1 ]);
        if (!is_null($already_exist))
            return $already_exist;
        else
        {
            $favorites =  $this->createPlayList(['name' => "Favorites", 'type_of' => 1]);
            $favorites->setDescription("My favorites movies");
            return $favorites;
        }
    }

}

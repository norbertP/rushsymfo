<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Api;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ApiRequest;

class MovieController extends AbstractController
{
        /**
     * @var UserController
     */
    private $user_controller;

    public function __construct(UserController $user_controller)
    {
        $this->user_controller = $user_controller;
    }
    /**
     * @Route("/movie/{id}", name="movie")
     */
    public function index($id, Request $request)
    {        
        $media_type = $request->get('media_type');
        $api = new Api();
        $request = new ApiRequest();
        if ($media_type == 'movie')
            $request->setResources("/movie/$id");
        elseif ($media_type == 'tv')
            $request->setResources("/tv/$id");

        $movie = $api->get($request);

        if($this->getUser()){
            $user_controller= $this->user_controller;
            $favorite_pl_id = $user_controller->getFavoriteList()->getIdDB();
        }
        else
        $favorite_pl_id = -1;

        $session = new Session();
        if (!is_null($session->get('movies_playlist')))
            [$movies_pl_map, $all_playlist] = [$session->get('movies_playlist'), $session->get('all_playlist')];
        else
            [$movies_pl_map, $all_playlist] =[[],[]];

        $param = 
        [
            'can_search' => false,
            'imagebase' => 'https://image.tmdb.org/t/p/w500',
            'movie' => $movie,
            'movies_pl_map' => $movies_pl_map,
            'all_playlist' => $all_playlist,
         'favorite_pl_id' => $favorite_pl_id
        ];

        return $this->render('movie/index.html.twig', $param);
    }
}

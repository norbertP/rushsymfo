<?php

namespace App\Controller;

use App\Entity\PlayList;
use App\Form\PlayListType;
use App\Repository\PlayListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Api;
use App\Entity\ApiRequest;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;



/**
 * @Route("/play/list")
 */
class PlayListController extends AbstractController
{
    /**
     * @Route("/", name="play_list_index", methods={"GET"})
     */
    public function index(PlayListRepository $playListRepository): Response
    {
        $criteria = ['IdOwner' => $this->getUser()->getId()];
        
        return $this->render('play_list/index.html.twig', [
            'play_lists' => $playListRepository->findBy($criteria),
        ]);
    }

    /**
     * @Route("/new", name="play_list_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $playList = new PlayList();
        $form = $this->createForm(PlayListType::class, $playList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($playList);
            $entityManager->flush();

            return $this->redirectToRoute('play_list_index');
        }

        return $this->render('play_list/new.html.twig', [
            'play_list' => $playList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="play_list_show", methods={"GET"})
     */
    public function show(PlayList $playList): Response
    {
        return $this->render('play_list/show.html.twig', [
            'play_list' => $playList,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="play_list_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PlayList $playList): Response
    {
        $form = $this->createForm(PlayListType::class, $playList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_list_index');
        }

        return $this->render('play_list/edit.html.twig', [
            'play_list' => $playList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="play_list_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PlayList $playList): Response
    {
        if ($this->isCsrfTokenValid('delete'.$playList->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($playList);
            $entityManager->flush();
        }

        return $this->redirectToRoute('play_list_index');
    }

    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(PlayListRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
       
        $this->em = $em;
    }



    public function getAllOf($param=['resource'=>'discover', 'resource_param' => []]) 
    {
        $api = new Api();
        $api_request = new ApiRequest();
        $api_request->setResources($param['resource'])
                    ->setParameters($param['resource_param']);

        $movies = $api->get($api_request);                    
        return $movies;
    }


    /**
     * 
     * @return PlayList
     */
    public function newPlayList($param) 
    {
        $id_user=$param['id_user'];
        $name_to_create=$param['name'];
        $type_of = $param['type_of'];
        $description = isset($param['description'])?$param['description']:"Please add a description for playlist of $name_to_create";
        $repository = $this->repository;

        $already_exist = $repository->findOneBy(["Name" => $name_to_create,
                                                     "IdOwner" => $id_user]);
        $new = null;
        if(is_null($already_exist))
        {


            $api = new Api();
            $request = new ApiRequest();
            $request->setResources('/list');
            $playlist_data = ["name" => $name_to_create, 
                                "description"=> $description,
                                "language"=> "fr"];
            $playlist_result = $api->post($request, $playlist_data, true);

            if ($playlist_result['success'])
            {
                $play_list_new = new PlayList();
                $em = $this->em;
                $play_list_new->setName($name_to_create)
                                ->setIdOwner($id_user)
                                ->setIdDb($playlist_result['list_id'])
                                ->setTypeOf($type_of)
                                ->setAccessibility(1)
                                ->setCreationAt(new DateTime())
                                ->setLastUpdateAt(new DateTime())
                                ->setDescription($description);
                $em->persist($play_list_new);
                $em->flush();            
                $new = $play_list_new;

                $session = new Session();
                $all_playlist = $session->get('all_playlist');
                $all_playlist[$new->getIdDB()] = $new ;
                $session->set('all_playlist',$all_playlist) ;

            }
        }

        return $new;
    }

     /**
     * @Route("/{iddb_list}/add/{id_movie}", name="playlist.add", requirements={"iddb_list":"[0-9]*"})
     * @return string
     */

    public function addMovieApi($iddb_list, $id_movie)
    {
        $playlist = $this->repository->findOneBy(['IdDB' => $iddb_list]);
        return $this->addMovie($playlist, $id_movie);
    }

    public function addMovie(PlayList $playlist, $id_movie)
    {       
        $api = new Api();
        $request = new ApiRequest();
        $id_list = $playlist->getIdDB();

        $request->setResources("/list/$id_list/add_item");
        $data = ["media_id" => $id_movie];
        $result = $api->post($request, $data, true);
        $result += ['id_list' => $id_list];

        $session = new Session();
        $list_new = $session->get('movies_playlist');
        if (isset($list_new[ $id_movie]))
            $list_new[ $id_movie] []= $playlist;
        else
            $list_new[ $id_movie] = [$playlist];
        $session->set('movies_playlist',$list_new) ;

        
        $response = new JsonResponse($result);
        return $response;
    }

/**
     * @Route("/{iddb_list}/remove/{id_movie}", name="playlist.remove", requirements={"iddb_list":"[0-9]*"})
     * @return string
     */

    public function removeMovieApi($iddb_list, $id_movie)
    {
        $playlist = $this->repository->findOneBy(['IdDB' => $iddb_list]);
        return $this->removeMovie($playlist, $id_movie);
    }

    public function removeMovie(PlayList $playlist, $id_movie)
    {       
        $api = new Api();
        $request = new ApiRequest();
        $id_list = $playlist->getIdDB();

        $request->setResources("/list/$id_list/remove_item");
        $data = ["media_id" => $id_movie];
        $result = $api->post($request, $data, true);
        $result += ['id_list' => $id_list];

        $session = new Session();
        $list_new = $session->get('movies_playlist');
        $list_sub_old = $list_new[ $id_movie];
        $list_sub_new = [];
        foreach($list_sub_old as $list)
        {
            if ($list->getId() != $playlist->getId())
                $list_sub_new[]=$list;
        }
        $list_new[ $id_movie] = $list_sub_new;

        $session->set('movies_playlist',$list_new) ;

        
        $response = new JsonResponse($result);
        return $response;
    }

}

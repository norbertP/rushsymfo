<?php

namespace App\Controller;

use App\Repository\PlayListRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use App\Entity\Api;
use App\Entity\ApiRequest;

class AdminController extends EasyAdminController
{
   /** @var array The full configuration of the entire backend */
   protected $config;
   /** @var array The full configuration of the current entity */
   protected $entity;
   /** @var Request The instance of the current Symfony request */
   protected $request;
   /** @var EntityManager The Doctrine entity manager for the current entity */
   protected $em;

   private $playlist_repo;
    /**
     * @var ObjectManager
     */
    
    public function __construct(PlayListRepository $playlist_repo, EntityManagerInterface $em)
    {
        $this->playlist_repo = $playlist_repo;

        $this->em = $em;
    }

   public function removeEntity($entity)
   {
      if (\get_class($entity) == "App\Entity\UserPlayList")
      {
         $api = new Api();
         $item_list = $this->playlist_repo->find(['id' => $entity->getIdPlayList()]); 
         foreach ([$item_list] as $item)
         {
            $this->em->remove($item);
            $IdDB = $item->getIdDB();
            $request = new ApiRequest();
            $request->setResources("/list/$IdDB");
            $result_list = $api->delete($request, true);
         }
         $this->em->flush();
      }

      if (\get_class($entity) == "App\Entity\PlayList")
      {
         $api = new Api();
         $item_list = $this->playlist_repo->find(['id' => $entity->getIdPlayList()]); 
         foreach ([$item_list] as $item)
         {
            $this->em->remove($item);
            $IdDB = $item->getIdDB();
            $request = new ApiRequest();
            $request->setResources("/list/$IdDB");
            $result_list = $api->delete($request, true);
         }
         $this->em->flush();
      }


       parent::removeEntity($entity);  
   }
}
<?php

namespace App\Form;

use App\Entity\PlayList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlayListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name')
            ->add('Description')
            //->add('Creation_at')
            //->add('IdOwner')
            //->add('LastUpdate_at')
            //->add('TypeOf')
            //->add('Accessibility')
            //->add('IdDB')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PlayList::class,
        ]);
    }
}

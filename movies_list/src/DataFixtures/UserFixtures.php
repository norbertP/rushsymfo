<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        
        $user = new User();
        $pwd = hash('sha512', '12345678');
        $manager->persist($user);
        $user->setName("Norbert")
            ->setEmail("aaa@aaa.com")
            ->setPassword($pwd);
        $manager->flush();
    }
}

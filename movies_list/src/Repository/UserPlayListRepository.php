<?php

namespace App\Repository;

use App\Entity\UserPlayList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserPlayList|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPlayList|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPlayList[]    findAll()
 * @method UserPlayList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPlayListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPlayList::class);
    }

    // /**
    //  * @return UserPlayList[] Returns an array of UserPlayList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPlayList
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

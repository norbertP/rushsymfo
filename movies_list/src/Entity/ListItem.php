<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ListItemRepository")
 */

 
class ListItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_play_list;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_movie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPlayList(): ?int
    {
        return $this->id_play_list;
    }

    public function setIdPlayList(int $id_play_list): self
    {
        $this->id_play_list = $id_play_list;

        return $this;
    }

    public function getIdMovie(): ?int
    {
        return $this->id_movie;
    }

    public function setIdMovie(int $id_movie): self
    {
        $this->id_movie = $id_movie;

        return $this;
    }
}

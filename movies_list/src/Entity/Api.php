<?php
namespace App\Entity;

use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpClient\NativeHttpClient;
use App\Entity\ApiRequest;

class Api
{
    private $token;
    private $session_id=null;

    public function authentification()
    {
        if (!is_null($this->session_id))
            return true;
        $request = new ApiRequest();

        $request->setResources('/authentication/token/new');
        
        $auth_result = $this->get($request);
        if ($auth_result['success'])
        {
            $request_token_temp = $auth_result['request_token']; 
            unset($request);
            $request = new ApiRequest();
            $request->setResources('/authentication/token/validate_with_login');
            //Log to Movies db
            $login_data = ["username" => "nornertP",
                            "password" => "azertyuiop&10",
                            "request_token" => $request_token_temp];
            $login_result = $this->post($request, $login_data);
            if ($login_result['success'])
            {
                $token=$login_result['request_token'];
                $this->setToken($token);
                unset($request);
                //Now we open a session
                $request = new ApiRequest();
                $request->setResources('/authentication/session/new');
                
                $session_data = ["request_token" => $token];
                $session_result = $this->post($request, $session_data);
                if ($session_result['success'])
                {
                    $this->setSession_id($session_result['session_id']);
                    $session = new Session();
                    //$session->start();
                    $session->set('db_session_id', $session_result['session_id']);
                    $session->set('db_token', $token);

                }
            }
        }
    }

    private function getSessionInformation()
    { 
        $session = new Session();
        //$session->start();
        if (is_null($session->get('db_session_id')))
        {
            $this->authentification();
        }
        $session_id = $session->get('db_session_id');
        $token = $session->get('db_token' );
        $this->session_id = $session_id;
        $this->token = $token;
        return ['session_id' => $session_id, 
            'token_id' => $token];
    }

    public function post($request, $post_data, $need_auth=false)
    {

        $return = [];
        $client = new NativeHttpClient();
        $post_param = ['json' => $post_data,
                    'headers' => ['content-Type' => 'application/json; charset=utf-8']];

        if ($need_auth)
        { 
            ['token_id' => $access_token, 'session_id' => $session_id] = $this->getSessionInformation();
            $post_param['headers'] += ['authorization' => "Bearer $access_token"];
            
        }
        if (!is_null($this->session_id))
            $request->setParameters(['session_id' => $session_id]);

        $request_str = $request->getRequest();
        try{
            $response = $client->request('POST', $request_str, $post_param);
            $return = $response->toArray();
        }
        catch (\Exception $e)
        {
            $return = ['success' => false, 'status_code'=>'ko', 'status_message' => $e->getMessage()];
        }
        return  $return;  
    }

    public function get($request, $need_auth=false)
    {
        $return= [];
        if ($need_auth)
        { 
            ['token_id' => $access_token, 'session_id' => $session_id] = $this->getSessionInformation();
            //$post_param += ['auth_bearer' => "$access_token"];
            $request->setParameters(['session_id' => $session_id]);
        }
        $request_str = $request->getRequest();
        $client = new NativeHttpClient(); //NativeHttpClient::create(['http_version' => '2.0']);
        try{
            $response = $client->request('GET', $request_str);
            $return = $response->toArray();
        }
        catch (\Exception $e)
        {
            $return = ['success' => false, 'status_code'=>'ko', 'status_message' => $e->getMessage()];
        }
        return  $return;
    }
    
    public function delete($request, $need_auth=false)
    {
        $return= [];
        if ($need_auth)
        { 
            ['token_id' => $access_token, 'session_id' => $session_id] = $this->getSessionInformation();
            $request->setParameters(['session_id' => $session_id]);
        }
        $request_str = $request->getRequest();
        $client = new NativeHttpClient(); 
        try{
            $response = $client->request('DELETE', $request_str);
            $return = $response->toArray();
        }
        catch (\Exception $e)
        {
            $return = ['success' => false, 'status_code'=>'ko', 'status_message' => $e->getMessage()];
        }
        return  $return;
    }

    /**
     * Get the value of token
     */ 
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set the value of token
     *
     * @return  self
     */ 
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get the value of session_id
     */ 
    public function getSession_id()
    {
        return $this->session_id;
    }

    /**
     * Set the value of session_id
     *
     * @return  self
     */ 
    public function setSession_id($session_id)
    {
        $this->session_id = $session_id;

        return $this;
    }
}
<?php
namespace App\Entity;
class ApiRequest
{
    private $url = "https://api.themoviedb.org/3";
    private $resources ="";
    private $parameters =[];
    private $method="GET";
    private $data=[];

     public function __construct()
    {
        $this->setParameters(['api_key' => '83142e4b2c007433690f2685af7e4541'])
            ->setParameters(['language' => 'fr']);
        
    }
    public function getRequest()
    {
        return $this->url  . $this->resources . "?" . http_build_query($this->parameters);
    }

    public function setLanguage($language)
    {
        $this->parameter['language'] = $language;
        return $this;
    }



    public function discover($param= ['sort_by' => 'popularity.desc'])
    {
        return $this->setResources("/discover/movie")
                    ->setParameters($param);
    }

    public function call()
    {
        $request = $this->getRequest();
        $array_global = json_decode(file_get_contents($request), true);
        //$result_nb = $array_global['totalResults'];
        //$result_status = $array_global['status'];
        //if ($result_status != 'ok')
        return $array_global;
    }
    


    /**
     * Get the value of parameters
     */ 
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set the value of parameters
     *
     * @return  self
     */ 
    public function setParameters($parameters)
    {
        $this->parameters += $parameters;

        return $this;
    }

    /**
     * Get the value of resources
     */ 
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * Set the value of resources
     *
     * @return  self
     */ 
    public function setResources($resources)
    {
        $this->resources = $resources;

        return $this;
    }

    /**
     * Get the value of method
     */ 
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set the value of method
     *
     * @return  self
     */ 
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get the value of data
     */ 
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @return  self
     */ 
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
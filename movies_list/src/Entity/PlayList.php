<?php

namespace App\Entity;
use App\Entity\Api;
use App\Entity\ApiRequest;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayListRepository")
 */
class PlayList
{

    public function remove()
    {
        $a=1;

    }
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description="";

    /**
     * @ORM\Column(type="datetime")
     */
    private $Creation_at;

    /**
     * @ORM\Column(type="integer")
     */
    private $IdOwner;

    /**
     * @ORM\Column(type="datetime")
     */
    private $LastUpdate_at;

    /**
     * @ORM\Column(type="integer")
     */
    private $TypeOf;

    /**
     * @ORM\Column(type="integer")
     */
    private $Accessibility;

    /**
     * @ORM\Column(type="integer")
     */
    private $IdDB;


    public function getShortName(): ?string
    {
        return substr($this->getName(),0,10);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getCreationAt(): ?\DateTimeInterface
    {
        return $this->Creation_at;
    }

    public function setCreationAt(\DateTimeInterface $Creation_at): self
    {
        $this->Creation_at = $Creation_at;

        return $this;
    }

    public function getIdOwner(): ?int
    {
        return $this->IdOwner;
    }

    public function setIdOwner(int $IdOwner): self
    {
        $this->IdOwner = $IdOwner;

        return $this;
    }

    public function getLastUpdateAt(): ?\DateTimeInterface
    {
        return $this->LastUpdate_at;
    }

    public function setLastUpdateAt(\DateTimeInterface $LastUpdate_at): self
    {
        $this->LastUpdate_at = $LastUpdate_at;

        return $this;
    }

    public function getTypeOf(): ?int
    {
        return $this->TypeOf;
    }

    public function setTypeOf(int $TypeOf): self
    {
        $this->TypeOf = $TypeOf;

        return $this;
    }

    public function getAccessibility(): ?int
    {
        return $this->Accessibility;
    }

    public function setAccessibility(int $Accessibility): self
    {
        $this->Accessibility = $Accessibility;

        return $this;
    }

    public function getIdDB(): ?int
    {
        return $this->IdDB;
    }

    public function setIdDB(int $IdDB): self
    {
        $this->IdDB = $IdDB;

        return $this;
    }
}

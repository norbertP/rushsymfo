<?php

namespace App\Entity;

use App\Repository\PlayListRepository;
use App\Repository\UserPlayListRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPlayListRepository")
 */
class UserPlayList
{
    const ROLE_OWNER=1;
    const ROLE_CONTRIBUTOR=2;
    const ROLE_READER=3;

    private $repository;
    private $pl_repository;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $IdPlayList;

    /**
     * @ORM\Column(type="integer")
     */
    private $IdUser;

    /**
     * @ORM\Column(type="integer")
     */
    private $Role;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPlayList(): ?int
    {
        return $this->IdPlayList;
    }

    public function setIdPlayList(int $IdPlayList): self
    {
        $this->IdPlayList = $IdPlayList;

        return $this;
    }

    public function getIdUser(): ?int
    {
        return $this->IdUser;
    }

    public function setIdUser(int $IdUser): self
    {
        $this->IdUser = $IdUser;

        return $this;
    }

    public function getRole(): ?int
    {
        return $this->Role;
    }

    public function setRole(int $Role): self
    {
        $this->Role = $Role;

        return $this;
    }
}

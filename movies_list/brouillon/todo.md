• Allow the user :
* to register
* to connect
* to edit their personal login information 
* Avec un bug => to unsubscribe from the site
* to create lists, edit its information (name, description), and to delete according to needs


• to search for films (see next point)
* • to have additional information about a film by clicking on it
• to add a film to favorites (automatically placed in the “favorite films” list)
• to remove a title from favorites (automatically deleted from the “favorite films” list)
• to add a movie to a particular list directly from the film page
• to delete a movie from the list
• to share movie lists to other users on the site
• Allow the user to search films by :
• name
• release date
• actors who star in the film
• type